<?php
/**
 * User Information
 *
 * @class     FCI_Information
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FCI_Information Class.
 */
class FCI_Information {

	/**
	 * Hook in methods.
	 */
	public static function init() {

		if ( !is_admin() ) {
			add_action( 'wp', array( __CLASS__, 'change_next_button' ) );
			add_action( 'wp', array( __CLASS__, 'check_if_needs_information' ) );
			add_action( 'wp', array( __CLASS__, 'process_information' ) );
		}
		add_shortcode( 'fjellcommerce_information', array( __CLASS__, 'shortcode' ) );
		add_action( 'woocommerce_product_options_advanced', array( __CLASS__, 'add_custom_fields' ) );
		add_action( 'woocommerce_process_product_meta', array( __CLASS__, 'save_custom_fields' ) );
		add_action( 'fjellcommerce_information_proceed', array( 'FCI_Information', 'proceed_button' ), 1 );
		add_action( 'woocommerce_checkout_update_order_meta', array( __CLASS__, 'checkout_meta' ), 10, 2 );
		add_filter( 'fjellcommerce_ticket_holder_name', array( __CLASS__, 'ticket_holder_name' ), 10, 5 );
		add_filter( 'fjellcommerce_ticket_holder_phone', array( __CLASS__, 'ticket_holder_phone' ), 10, 5 );
		add_filter( 'fjellcommerce_ticket_holder_email', array( __CLASS__, 'ticket_holder_email' ), 10, 5 );
		add_filter( 'fjellcommerce_ticket_holder_comment', array( __CLASS__, 'ticket_holder_comment' ), 10, 5 );

		// modify order
		add_action('fjellcommerce_edit_ticket_form', array(__CLASS__, 'edit_ticket_holder_form'), 20, 3);
		add_action('fjellcommerce_edit_ticket_save', array(__CLASS__, 'save_ticket_holder_form'), 20, 4);
	}

	/**
	 * Saves the information for a ticket
	 * @param  array $ticket
	 * @param  integer $ticket_id
	 * @param  integer $order_id
	 * @param  array $postdata
	 * @return void
	 */
	public static function save_ticket_holder_form($ticket, $ticket_id, $order_id, $postdata) {
		$info = $postdata['user_info'][$ticket_id];
		$current_info = get_post_meta($order_id, '_information_data', true);
		if(!$current_info) {
			$current_info = array();
		}
		if(!isset($current_info[$ticket['order_item']['variation_id']])) {
			$current_info[$ticket['order_item']['variation_id']] = array();
		}
		$current_info[$ticket['order_item']['variation_id']][$ticket_id] = $info;
		update_post_meta( $order_id, '_information_data', $current_info );
	}

	/**
	 * Renders a form for editing user information for tickets
	 * @param  array $ticket
	 * @param  integer $ticket_id
	 * @param  WC_Order $order
	 * @return void
	 */
	public static function edit_ticket_holder_form($ticket, $ticket_id, $order) {
		$name = trim($order->billing_first_name . ' ' . $order->billing_last_name) ? $order->billing_first_name . ' ' . $order->billing_last_name : 'Deltaker uten navn';
		$email = $order->billing_email ? $order->billing_email : ' ';
		$phone = $order->billing_phone ? $order->billing_phone : ' ';
		$comment = ' ';
		$data = array(
			'name'    => apply_filters('fjellcommerce_ticket_holder_name', $name, $order->id, $ticket['order_item']['product_id'], $ticket['order_item']['variation_id'], $ticket_id),
			'email'   => apply_filters('fjellcommerce_ticket_holder_email', $email, $order->id, $ticket['order_item']['product_id'], $ticket['order_item']['variation_id'], $ticket_id),
			'phone'   => apply_filters('fjellcommerce_ticket_holder_phone', $phone, $order->id, $ticket['order_item']['product_id'], $ticket['order_item']['variation_id'], $ticket_id),
			'comment' => apply_filters('fjellcommerce_ticket_holder_comment', $comment, $order->id, $ticket['order_item']['product_id'], $ticket['order_item']['variation_id'], $ticket_id)
		);
		?>
		<div class="ticket_edit_part">
			<strong>Personalia</strong>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					Navn
				</div>
				<div class="ticket_edit_part_input">
					<input type="text" class="widefat" name="user_info[<?php echo $ticket_id ?>][name]" value="<?php echo $data['name'] ?>">
				</div>
			</div>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					E-post
				</div>
				<div class="ticket_edit_part_input">
					<input type="text" class="widefat" name="user_info[<?php echo $ticket_id ?>][email]" value="<?php echo $data['email'] ?>">
				</div>
			</div>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					Telefon
				</div>
				<div class="ticket_edit_part_input">
					<input type="text" class="widefat" name="user_info[<?php echo $ticket_id ?>][phone]" value="<?php echo $data['phone'] ?>">
				</div>
			</div>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					Kommentar
				</div>
				<div class="ticket_edit_part_input">
					<textarea class="widefat" name="user_info[<?php echo $ticket_id ?>][comment]" rows="10"><?php echo $data['comment'] ?></textarea>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Outputs the name for a ticket holder
	 * @param  string $name
	 * @param  integer $order_id
	 * @param  integer $product_id
	 * @param  integer $variation_id
	 * @param  integer $ticket_id
	 * @return string
	 */
	public static function ticket_holder_name( $name, $order_id, $product_id, $variation_id, $ticket_id ) {
		$names = get_post_meta( $order_id, '_information_data', true );
		$order = wc_get_product( $product_id );
		if ( isset( $names[$variation_id] ) && isset( $names[$variation_id][$ticket_id] ) && isset($names[$variation_id][$ticket_id]['name']) )
			return $names[$variation_id][$ticket_id]['name'];
		return $name;

	}

	/**
	 * Outputs the phone for a ticket holder
	 * @param  string $phone
	 * @param  integer $order_id
	 * @param  integer $product_id
	 * @param  integer $variation_id
	 * @param  integer $ticket_id
	 * @return string
	 */
	public static function ticket_holder_phone( $phone, $order_id, $product_id, $variation_id, $ticket_id ) {
		$names = get_post_meta( $order_id, '_information_data', true );
		$order = wc_get_product( $product_id );
		if ( isset( $names[$variation_id] ) && isset( $names[$variation_id][$ticket_id] ) && isset($names[$variation_id][$ticket_id]['phone']) )
			return $names[$variation_id][$ticket_id]['phone'];
		return $phone;

	}

	/**
	 * Outputs the email for a ticket holder
	 * @param  string $email
	 * @param  integer $order_id
	 * @param  integer $product_id
	 * @param  integer $variation_id
	 * @param  integer $ticket_id
	 * @return string
	 */
	public static function ticket_holder_email( $email, $order_id, $product_id, $variation_id, $ticket_id ) {
		$names = get_post_meta( $order_id, '_information_data', true );
		$order = wc_get_product( $product_id );
		if ( isset( $names[$variation_id] ) && isset( $names[$variation_id][$ticket_id] ) && isset($names[$variation_id][$ticket_id]['email']) )
			return $names[$variation_id][$ticket_id]['email'];
		return $email;

	}

	/**
	 * Outputs the comment for a ticket holder
	 * @param  string $comment
	 * @param  integer $order_id
	 * @param  integer $product_id
	 * @param  integer $variation_id
	 * @param  integer $ticket_id
	 * @return string
	 */
	public static function ticket_holder_comment( $comment, $order_id, $product_id, $variation_id, $ticket_id ) {
		$names = get_post_meta( $order_id, '_information_data', true );
		$order = wc_get_product( $product_id );
		if ( isset( $names[$variation_id] ) && isset( $names[$variation_id][$ticket_id] ) && isset($names[$variation_id][$ticket_id]['comment']) )
			return $names[$variation_id][$ticket_id]['comment'];
		return $comment;

	}

	/**
	 * Renders the proceed button
	 * @return void
	 */
	public static function proceed_button() {
?>
		<button type="submit" class="checkout-button button alt wc-forward">
			<?php echo __( 'Proceed to Checkout', 'woocommerce' ); ?>
		</button>
		<?php
	}

	/**
	 * Saves the field value from the WC admin screen
	 * @param  integer $post_id
	 * @return void
	 */
	public static function save_custom_fields( $post_id ) {
		$woocommerce_checkbox = isset( $_POST['_user_information'] ) ? 'yes' : 'no';
		update_post_meta( $post_id, '_user_information', $woocommerce_checkbox );
	}

	/**
	 * Adds a checkbox to the WC admin screen
	 */
	public static function add_custom_fields() {
		// Print a custom text field
		woocommerce_wp_checkbox( array(
				'id' => '_user_information',
				'label' => __( 'Add User Information Fields', 'fjellcommerce-information' ),
				'description' => __( 'Select whether or not you want to require user information for this product', 'fjellcommerce-information' )
			) );
	}

	/**
	 * Changes the next button so the next step after the cart will be information if needed
	 * @return void
	 */
	public static function change_next_button() {
		if ( self::requires_information() ) {
			remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
			// remove_action( 'fjellcommerce_seatmap_proceed', array( 'FCS_Seatmap', 'proceed_button' ), 1 );
			add_action( 'woocommerce_proceed_to_checkout', array( 'FCI_Information', 'button_proceed_to_information' ), 20 );
			add_filter( 'fjellcommerce_seatmap_proceed_url', function($url) {
				return self::get_information_url();
			}, 20 );
			// add_action( 'fjellcommerce_seatmap_proceed', array( 'FCI_Information', 'button_proceed_to_information' ), 1 );
		}
	}

	/**
	 * Checks if we're at checkout and we still need information.
	 *
	 * Redirects if the user is not done.
	 *
	 * @return void
	 */
	public static function check_if_needs_information() {
		if ( is_checkout() && self::needs_information() ) {
			wp_redirect( self::get_information_url() ); exit;
		}
	}

	/**
	 * Do we still need information from the user?
	 * @return boolean
	 */
	public static function needs_information() {
		$information = WC()->session->get( 'information_data', array() );
		$error = 0;
		if($information) {
			foreach ( $information as $cart_id => $users ) {
				foreach($users as $user) {
					if(!$user['name'] || !$user['phone'] || !$user['email']) {
						$error++;
					}
				}
			}
		}
		if($error) {
			return self::requires_information() && true;
		}
		return self::requires_information() && !$information;
	}

	/**
	 * Does anything in the cart need information?
	 * @return boolean
	 */
	public static function requires_information() {
		$posts = array_filter( WC()->cart->get_cart(), function( $item ) {
				$post = $item['data']->post;
				if ( $post->_user_information && $post->_user_information === 'yes' ) {
					return true;
				}
				return false;
			} );
		if ( count( $posts ) > 0 ) {
			return true;
		}

		return false;
	}

	/**
	 * Renders the button to proceed to entering information
	 * @return void
	 */
	public static function button_proceed_to_information() { ?>
		<a href="<?php echo esc_url( self::get_information_url() ) ;?>" class="information-button nff-checkout-button nff-button alt wc-forward">
			<?php echo __( 'Legg inn deltakerinformasjon', 'fjellcommerce-information' ); ?>
		</a>
		<?php
	}

	/**
	 * Gets the URL to the information page
	 * @return string
	 */
	public static function get_information_url() {
		return get_permalink( get_option( 'fjellcommerce_information_page', false ) );
	}

	/**
	 * Renders the shortcode for information
	 * @return string
	 */
	public static function shortcode() {
		$posts = array();
		foreach ( WC()->cart->get_cart() as $key => $item ) {
			if($item['data']->post->_user_information === 'yes') {
				$posts[] = array(
					'post' => $item['data']->post,
					'key' => $key,
					'quantity' => $item['quantity'],
					'item' => $item
				);
			}
		}

		include dirname( FCI_PLUGIN_FILE ) . '/templates/information.php';
		$content = ob_get_clean();

		return $content;
	}

	/**
	 * Processes the information supplied by the user
	 * @return void
	 */
	public static function process_information() {
		if ( $_POST && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'fjellcommerce_information' ) ) {
			self::process_information_action();
			$url = apply_filters( 'fjellcommerce_information_proceed_url', wc_get_checkout_url() );
			wp_redirect( $url ); exit;
		}
	}

	public static function process_information_action() {
		$products = $_POST['user_info'];
		WC()->session->set( 'information_data', $products );
	}

	/**
	 * Saves the user information once the order has been placed and resets it.
	 * @param  integer $order_id
	 * @param  array $posted
	 * @return void
	 */
	public static function checkout_meta( $order_id, $posted ) {
		if ( self::requires_information() ) {
			$information = WC()->session->get( 'information_data', array() );
			$cart = WC()->cart->get_cart();
			$user_info = array();
			foreach ( $information as $cart_id => $users ) {
				$cart_item = $cart[$cart_id];
				$product_id = isset( $cart_item['variation_id'] ) ? $cart_item['variation_id'] : $cart_item['product_id'];
				$user_info[$product_id] = $users;
			}
			update_post_meta( $order_id, '_information_data', $user_info );
			WC()->session->set( 'information_data', array() );
		}
	}


}

FCI_Information::init();
