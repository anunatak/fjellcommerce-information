<?php foreach ( $posts as $product ) : ?>
	<div class="events">
		<h3><?php echo get_the_title( $product['post']->ID ) ?></h3>
		<?php if ( isset( $product['item']['variation'] ) ) : ?>
			<div class="event-information">
				<?php $term = get_term_by( 'slug', $product['item']['variation']['attribute_pa_ticket_type'], 'pa_ticket_type' ); ?>
				<?php echo $term->name ?>
			</div>
		<?php endif ?>
		<?php for ( $i=1; $i <= $product['quantity']; $i++ ) { ?>
			<small>Deltaker <?php echo $i ?></small>
			<div class="user-information">
				<div class="user-field half">
					<label>Fullt navn</label>
					<input required="" class="user-name" type="text" name="user_info[<?php echo $product['key'] ?>][<?php echo $i ?>][name]">
				</div>
				<div class="user-field half">
					<label>Telefon</label>
					<input required="" class="user-phone" type="tel" name="user_info[<?php echo $product['key'] ?>][<?php echo $i ?>][phone]">
				</div>
				<div class="user-field half">
					<label>Epost</label>
					<input required="" class="user-mail" type="email" name="user_info[<?php echo $product['key'] ?>][<?php echo $i ?>][email]">
				</div>
				<div class="user-field half">
					<label>Kommentar</label>
					<input class="user-comment" type="text" name="user_info[<?php echo $product['key'] ?>][<?php echo $i ?>][comment]">
				</div>

			</div>
		<?php } ?>

	</div>
<?php endforeach ?>
