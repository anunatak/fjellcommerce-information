<form action="" method="POST">
<div class="woocommerce fjellcommerce-information">
	<p><?php _e( 'En eller flere av dine produkter krever at du legger inn deltakerinformasjon.', 'fjellcommerce-seatmap' ) ?></p>


		<?php wp_nonce_field( 'fjellcommerce_information' ) ?>

		<?php include 'information_form.php'; ?>
</div>

<div class="shopping-actions">
	<div class="cart-total">
		<div class="cart-total-title">
			Total sum
		</div>
		<div class="cart-total-number">
			<?php wc_cart_totals_subtotal_html() ?>
		</div>
	</div>
	<div class="action-buttons">
		<a href="<?php echo esc_url( home_url( '/program' ) ) ?>" class="button shop-more">Handle videre</a>
		<?php do_action( 'fjellcommerce_information_proceed' ) ?>
	</div>
</div>


</form>
